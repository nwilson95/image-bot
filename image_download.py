from tkinter import *
import praw
# import openpyxl
import os
import time
import sys

m = Tk()

m.title("Image Downloader")
def submit():
    import time
    from tkinter import messagebox
    import os
    reddit_id = os.environ.get('reddit_id')
    sub = e_sub.get()
    number = e_count.get()
    dir = e_directory.get()
    #time = time.strftime("%Y/%m/%d - ")
    reddit = praw.Reddit(client_id=reddit_id,
    client_secret=os.environ.get('reddit_secret'),
    user_agent=os.environ.get('reddit_agent'),
    username=os.environ.get('reddit_user'),
    password=os.environ.get('reddit_password'))
    import urllib.request
    if not os.path.exists(dir):
        os.makedirs(dir)
    subreddit = reddit.subreddit(sub)
    count = 0

    if number.isdigit():
        for submission in subreddit.top(limit=int(number)):
            filename = os.path.join(dir, f"image{count}.jpeg")
            url = str(submission.url)
            if url.endswith("jpg") or url.endswith("jpeg") or url.endswith("png"):
                urllib.request.urlretrieve(url, filename)
                count += 1
        messagebox.showinfo("Window", "Finished")
    else:
        messagebox.showinfo("Window", "Please enter a number.")

l_sub = Label(m, text="Subreddit")
l_count = Label(m, text="Number")
l_directory = Label(m, text="Save to")
e_sub = Entry(m)
e_count = Entry(m)
e_directory = Entry(m)
button = Button(m, text="Submit", fg="black", command=submit)

l_sub.grid(row=0, sticky=E)
l_count.grid(row=1, sticky=E)
l_directory.grid(row=2, sticky=E)
e_sub.grid(row=0, column=1)
e_count.grid(row=1, column=1)
e_directory.grid(row=2, column=1)
button.grid(columnspan=2)



m.mainloop()
