# Image Downloader
I created this program to download images from any provided subreddit. It allows you to specify the subreddit, how many images (does this based off of "hot"), and where to save them. 
## Required
You will need to set enviroment variables for each value in the script. You can find out how to do this at the below links
* [Calling enviromental variables in python - Windows](https://www.youtube.com/watch?v=IolxqkL7cD8&app=desktop)
* [Calling enviromental variables in python - Mac/Linux](https://www.youtube.com/watch?v=5iWhQWVXosU)
### Variables to set
* reddit_secret (bot secret)
* reddit_id (bot id)
* reddit_agent (example u/user bot for downloading images.)
* reddit_user (your reddit username)
* reddit_password (your reddit password)
### [Reddit App](https://www.reddit.com/prefs/apps)
You will want to create a reddit app for this bot. This bot uses your "ID" for the bot and your "secret". When creating the app you want
to select the "script" option as this will be a script for personal use.
## Other Documentation
* [Praw documentation](https://praw.readthedocs.io/en/latest/)
* [Reddit API](https://www.reddit.com/dev/api)
